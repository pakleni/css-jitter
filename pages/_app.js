import React from "react";
import "../css/style.css";

import Nav from "../components/nav";

function MyApp({ Component, pageProps }) {
  return (
    <div>
      <div>
        <Nav />
        <Component {...pageProps} />
      </div>
    </div>
  );
}

export default MyApp;
