import React from "react";
import Link from "next/link";

function Nav() {
  return (
    <nav className="navbar">
      <div className="navbar-brand">
        <Link href="/">
          <a className="navbar-item">
            <img src="/trinity-logo.png" />
          </a>
        </Link>
      </div>

      <div className="navbar-menu">
        <div className="navbar-start">
          <Link href="/">
            <a className="navbar-item underline">
              <b>Hover</b>
            </a>
          </Link>
          <Link href="/">
            <a className="navbar-item underline">
              <b>Hover</b>
            </a>
          </Link>
          <Link href="/">
            <a className="navbar-item underline">
              <b>Hover</b>
            </a>
          </Link>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
